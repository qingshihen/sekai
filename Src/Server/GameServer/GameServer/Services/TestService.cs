﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Network;
using SkillBridge.Message;

namespace GameServer.Services
{
    class TestService:Singleton<TestService>
    {
        public void Init()
        {


        }

        public void Start()
        {
            //接收
            MessageDistributer<NetConnection<NetSession>>.Instance.Subscribe<FirstTestRequest>(this.OnFirstTestRequest);
        }

        private void OnFirstTestRequest(NetConnection<NetSession> sender, FirstTestRequest message)
        {
            Log.InfoFormat("这是一个测试 从客户端发送的数据{0}", message.HelloLHJ);
        }

        public void Stop()
        {

        }
    }
}
