﻿using Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Login : MonoBehaviour {

	// Use this for initialization
	void Start () {
        NetClient.Instance.Init("127.0.0.1",8000);

        NetClient.Instance.Connect();

        //所有协议的前缀
        SkillBridge.Message.FirstTestRequest firstTestRequest = new SkillBridge.Message.FirstTestRequest();

        //消息的封装
        SkillBridge.Message.NetMessage msg = new SkillBridge.Message.NetMessage();
        msg.Request = new SkillBridge.Message.NetMessageRequest();
        msg.Request.firstRequest = new SkillBridge.Message.FirstTestRequest();
        msg.Request.firstRequest.HelloLHJ = "你好 世界";
        NetClient.Instance.SendMessage(msg);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
